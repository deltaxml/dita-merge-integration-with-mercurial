// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

public class MercurialCheckConflict {
  
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      DocumentBuilder db = dbf.newDocumentBuilder();
      try {
        
        Document doc = db.parse(new File(args[0]));
        Element root=doc.getDocumentElement();
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr1 = xpath.compile("/"+root.getNodeName()+"//*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        XPathExpression expr2 = xpath.compile("/"+root.getNodeName()+"//@*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        XPathExpression expr3 = xpath.compile(root.getNodeName()+"/@*[local-name()='deltaV2']");
        XPathExpression expr4 = xpath.compile(root.getNodeName()+"/@*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        
        NodeList nl1= (NodeList) expr1.evaluate(doc, XPathConstants.NODESET);
        NodeList nl2 = (NodeList) expr2.evaluate(doc, XPathConstants.NODESET);
        String root_delta= expr3.evaluate(doc);
        NodeList root_attributes = (NodeList) expr4.evaluate(doc, XPathConstants.NODESET);
        
        if (nl2.getLength()==root_attributes.getLength() && !(root_delta.contains("!="))) {
          System.exit(0);
        } else if (nl1.getLength() > 0 || nl2.getLength() > 0) {
          System.exit(1);
        } else {
          System.exit(0);
        }
      } catch (org.xml.sax.SAXParseException e) {
        System.exit(1);
      }
    }
}