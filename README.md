## Integration with Mercurial

This sample is provided to show XML Merge integration with mercurial (hg) for merge, graft and update operations.    
    
For further details see our web page [Integration with Mercurial](https://docs.deltaxml.com/dita-merge/latest/samples-and-guides/integration-with-mercurial)    

 In order to use DeltaXML Merge with Mercurial XML please configure your Mercurial installation
 using the following steps:

 (a) in the `[merge-tools]` section specify the following and adjust the `/usr/local` path in the example to correspond to your merge installation.      
     This can be on a per-user basis in ~/.hgrc or per repository/site - see the Files section from:  `hg help config`      
     

    [merge-tools]
    DeltaXML-DITA-Merge.executable = /usr/local/DeltaXML-DITA-Merge-x_y_z_j/samples/MercurialMergePlugin/mercurial-merge.sh
    DeltaXML-DITA-Merge.args= $base $local $other $output ResultType=CONFLICTING_CHANGES WordByWord=true debug=false
    DeltaXML-DITA-Merge.binary = False
    DeltaXML-DITA-Merge.premerge = False
    DeltaXML-DITA-Merge.symlinks = False
    DeltaXML-DITA-Merge.gui = False

 The example above specifies pipeline parameters after the four filename args using param=value notation
 
 Note: The parameter 'ResultType' works well in such cases with one of the following settings available at commandline: 
 
 
 
	 1. SIMPLIFIED_DELTAV2
     2. SIMPLIFIED_RULE_PROCESSED_DELTAV2
     3. CONFLICTING_CHANGES
     4. THEIR_CHANGES
     5. ALL_CHANGES


 And then (b) in the `[merge-patterns]` section specify file types to use with DeltaXML-DITA-Merge:    
 

    [merge-patterns] 
    **.xml = DeltaXML-DITA-Merge

 Finally (c) in the `[ui]` section make the internal merge the default for other types of file:    
 

    [ui]
    merge = internal:merge

 Usage: As per:  https://mercurial.selenic.com/wiki/MergeToolConfiguration

 To merge from command line following command can be used.
    `$ hg merge branch` 

 A small java program `MercurialCheckConflict.java` checks for conflicts and returns   
 correct exit status. 1 --> For conflict and 0--> No conflict (Successful merge)
